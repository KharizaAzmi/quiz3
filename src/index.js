import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import Login from './Login';
import Home from './Home';

const Stack = createStackNavigator();

export default function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={Home} options={{ headerTitle: 'Daftar Barang' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const Login = () => (
  <Tab.Navigator>
    <Tab.Screen name="HomeScreen" component={Home} />
  </Tab.Navigator>
);

const Home = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="App" component={Home} />
    <Drawer.Screen name="AboutScreen" component={AboutScreen} />
  </Drawer.Navigator>
);
