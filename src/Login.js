import React, { useState } from 'react';
import { Image, StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export default function Login({ navigation }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('12345678');
  const [isError, setIsError] = useState(false);

  const submit = async () => {
    //tuliskan coding disini
    //? #Soal No. 1 (15poin) -- LoginScreen.js -- Function LoginScreen
    //? Buatlah sebuah fungsi untuk berpindah halaman hanya jika password yang di input bernilai '12345678'
    //? dan selain itu, maka akan mengubah state isError menjadi true dan tidak dapat berpindah halaman.
    //? #SoalTambahan (+ 5 poin): kirimkan params dengan key => userName dan value => this.state.userName ke halaman Home,
    //? dan tampilkan userName tersebut di halaman Home setelah teks "Hai," -->
    //end coding

    const _authData = await navigation.navigate('', '12345678');

    navigate(_authData);
    // if (password === '12345678') {
    //   setIsError(false);
    //   navigation.navigate('Home');
    //   return;
    // }
    // let dataToSend = { username: username, password: password };
    // let formBody = [];
    // for (let key in dataToSend) {
    //   let encodedKey = encodeURIComponent(key);
    //   let encodedValue = encodeURIComponent(dataToSend[key]);
    //   formBody.push(encodedKey + '=' + encodedValue);
    // }
    // formBody = formBody.join('&');
    // fetch('http://localhost:3000/api/user/login', {
    //   method: 'POST',
    //   body: formBody,
    //   headers: {
    //     //Header Defination
    //     'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    //   },
    // })
    //   .then((response) => response.json())
    //   .then((responseJson) => {
    //     //Hide Loader
    //     setIsError(false);
    //     console.log(responseJson);
    //     // If server response message same as Data Matched
    //     if (responseJson.status === 'success') {
    //       AsyncStorage.setItem('user_id', responseJson.data.username);
    //       console.log(responseJson.data.username);
    //       navigation.replace('DrawerNavigationRoutes');
    //     } else {
    //       setErrortext(responseJson.msg);
    //       console.log('Please check your username or password');
    //     }
    //   })
    //   .catch((error) => {
    //     //Hide Loader
    //     setIsError(true);
    //     console.error(error);
    //   });
  };
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 20, fontWeight: 'bold' }}>== Quiz 3 ==</Text>
      <Image style={{ height: 150, width: 150 }} source={require('../assets/logo.jpg')} />
      <View>
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Username"
          value={username}
          onChangeText={(value) => setUsername(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Password"
          value={password}
          onChangeText={(value) => setPassword(value)}
        />
        <Button title="Login" onPress={() => submit({ username, password })} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
